---
id: 19
title: 'Why Isn&#8217;t My Perfectly Written HTML Anchor Element Linking to Anything?'
date: '2015-10-01T19:10:13+02:00'
author: Patrick
layout: post
guid: 'http://patrickriggs.com/blog/?p=19'
permalink: /why-isnt-my-perfectly-written-html-anchor-element-linking-to-anything/
categories:
    - IT
tags:
    - CSS
    - development
    - HTML
    - 'mistakes were made'
    - webdev
---

Check your CSS z-indexes. Something is covering up your anchor.

For some forgotten reason, I had a banner header’s z-index set at -1. Cut several weeks later, my simple [ wasn’t working — it wasn’t clickable. It was the z-index of the containing ](/)

<div>. Herp a derp.</div>