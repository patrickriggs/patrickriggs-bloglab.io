---
id: 23
title: 'Tardy to the Party'
date: '2015-12-11T07:04:14+01:00'
author: Patrick
layout: post
guid: 'http://patrickriggs.com/blog/?p=23'
permalink: /tardy-to-the-party/
image: /wp-content/uploads/2015/12/cash-1.jpg
categories:
    - General
tags:
    - dad
    - hindsight
    - 'Johnny Cash'
    - music
---

Today at work, I found myself discussing with a colleague the concept of missing the boat on things you later in life can’t believe you wasted so much time on … denying.

The first example I went to was Johnny Cash. My father loved Cash. I remember as far back as age 6 or 7 and my dad’s friends asking me if I was “the man in black” whenever I wore dark clothing. I didn’t know what this reverence-based amusement was all about. All I knew was that Johnny Cash was that pudgy old man with the bad hair from that record cover and he was stupid.

I didn’t get into Cash in until I was around 20 years old. Rick Ruben was squeezing American III out of him by then and I was about 5 years too late to share the experience with my father.

For reasons unbeknownst to me, I have a habit of being stand-offish about things to which other people gravitate or are excited about. I was late to the party with The Clash and The Ramones just as I was late to JavaScript and frameworks. These aren’t examples of just being too young or being in the wrong place. These are things that were regularly placed in front of me that I denied because whoever put them there was foisting them upon me and I didn’t appreciate them getting their passion all over my shoes. Jealousy, maybe? That I didn’t know about X first?

This “anti” streak still runs through me 30 years later and really cost me many good years of sharing something my dad and I could have enjoyed together. Even before my son arrived, I’d been dwelling on this *trait*. I don’t understand where it comes from but I know what it has cost me. I’ll have to watch for it in my son.