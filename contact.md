---
id: 518
title: Contact
date: '2022-07-25T18:32:54+02:00'
author: Patrick
layout: page
guid: 'https://patrickriggs.com/blog/?page_id=518'
---

E-Mail: phriggs at gmail dot com

Twitter: [@patrickriggs](https://twitter.com/patrickriggs)

LinkedIn: <https://www.linkedin.com/in/patrick-riggs/>