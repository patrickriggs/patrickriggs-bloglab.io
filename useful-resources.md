---
id: 297
title: 'Useful Resources'
date: '2019-08-08T14:12:49+02:00'
author: Patrick
layout: page
guid: 'http://patrickriggs.com/blog/?page_id=297'
---

Personal bookmarks and resources I want at my fingertips anywhere I go. Primarily infosec-related content, here.

- <https://lolbas-project.github.io/> – Living off the Land Binaries and Scripts. Windows.
- <https://gtfobins.github.io/> – Curated list of Unix binaries that can be exploited by an attacker to bypass local security restrictions. “Living off the land.”
- <https://amanhardikar.com/mindmaps.html> – Aman Hardikar Mind Maps.
- [cheat sheet filetype:pdf site:sans.org](https://www.google.com/search?biw=1273&bih=598&ei=i59JXbfcK9G2ggeUvquQCw&q=cheat+sheet+filetype%3Apdf+site%3Asans.org&oq=cheat+sheet+filetype%3Apdf+site%3Asans.org&gs_l=psy-ab.3...94589.95339..96062...0.0..0.224.600.5j0j1......0....1..gws-wiz.OUdCLLwI0k8&ved=0ahUKEwj3zea1yu7jAhVRm-AKHRTfCrIQ4dUDCAo&uact=5) – Google search straight to all the SANS Institute PDF cheat sheets.
- <http://www.openwall.com/wordlists/ >– Wordlists for password cracking.
- <http://patorjk.com/software/taag/> – ASCII Art for your /etc/motd